'use client'

import {useState} from "react";
import axios from "axios";

export default function AlinkPage() {
    const [question, setQuestion] = useState('')
    const [response, setResponse] = useState('')

    const handleQuestion = e => {
        setQuestion(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()

        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyBIyFKDg5lSdJWsIR3nR6-0AJmFv39TI7U'

        const data = {"contents":[{"parts":[{"text": question}]}]}
        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={question} onChange={handleQuestion} placeholder="질문하세요"/>
                <button type="submit">확인</button>
            </form>
            {response && <p>답변은 : {response}</p>}
        </div>
    )
}