import Image from "next/image";
import {bgBlack} from "next/dist/lib/picocolors";

export default function Home() {
  return (
      <div className="flex min-h-screen flex-col items-center justify-between p-24">
        <div>
            <Image src={"/image.jpg"} alt={"image"} width={1024} height={768} />
            <div>너 얼마나 알고있니?</div>
        </div>
      </div>
  );
}
